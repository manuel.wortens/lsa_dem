import numpy as np
import pandas as pd
import itertools
import re


ASE = pd.read_excel('sinant.xlsx', index_col=0)

VECS = pd.DataFrame(columns = ['VOC','AVOCL','V0C+SIN','V+S_SVD','SIN','ASINL','SIN+ANT','S+A_SVD','ANT','AANTL','VOC+ANT','V+A_SVD'])

TAGS = pd.ExcelFile("TAGS.xlsx")

ace = []

lemas = []

LIMP ='(\!|\¡|\?|\¿|,|\"|\<|\>|\;|\:|\(|\)|\-|\_|\+|\=|\%|\.|\“|\—|\”|\/)'

PARENTESIS = '\(.+\)'

#-----------
def complejas(string):
    x = pd.read_excel(TAGS, string)
    x = x.loc[x[string].str.contains("_")]
    x = [[r[0].replace("_"," ")] for i, r in x.iterrows()]
    return x
data_complejas = [] #lista de expresiones complejas
s = list(map(complejas, ['PRO','PREP','CONJ','DET']))
for x in s:
    for y in x:
        data_complejas.append(y)

def limpieza(x):
    x = x.replace("\t", " ")
    x = x.replace("\n", " ")
    if "  " in x:
        x = x.replace("  "," ")
    return x.lower()

def matriz_de_conteo(ace):
    bolsa = ' '.join(ace)
    bolsa = bolsa.split()
    bolsa = list(dict.fromkeys((bolsa)))

    df = pd.DataFrame(index=bolsa)

    n = 1
    for frase in ace:
        print ("\tContando palabras de la acepción",n,"de un total de",len(ace),"...", end='\r')
        conteo = []

        for palabra in bolsa:
            palabra = r"\b" + re.escape(palabra) + r"\b"
            conteo.append(len(re.findall(palabra, frase)))
        df[frase] = conteo
        n = n+1
    print ("\n\t\tCuenta de palabras terminado.")
    return df.sort_index()

def coseno(tuplas,matriz):
    df = pd.DataFrame(columns = ['ACEPCIONES' , 'COS'])
    n = 1
    for x,y in tuplas:
        print ("\tCalculando coseno la bina número",n,"de un total de",len(tuplas),"...", end='\r')
        a = matriz[x]
        b = matriz[y]
        dot = np.dot(a, b)
        norma = np.linalg.norm(a)
        normb = np.linalg.norm(b)
        cos = dot / (norma * normb)
        titulo = x,y
        titulo = ' CON '.join(titulo)
        df = df.append({'ACEPCIONES' : titulo , 'COS' : cos} , ignore_index=True)
        n = n+1
    print ("\n\t\tCálculo de coseno terminado.")
    return df

#-----------

#----------- CREANDO LISTA DE PARO DE LA LISTA DE PALABRAS FUNCIÓN DEL DEM
lista = pd.read_excel(TAGS, 'CONJ', usecols=['CONJ']).values.tolist()
lista.extend(pd.read_excel(TAGS, 'PREP', usecols=['PREP']).values.tolist())
lista.extend(pd.read_excel(TAGS, 'DET', usecols=['DET']).values.tolist())
lista.extend(pd.read_excel(TAGS, 'PRO', usecols=['PRO']).values.tolist())
lista.extend(["1","2"]) #agregar palabras, número o lo que sea a la lista de paro
lista = ["".join(i) for i in lista]
lista = list(dict.fromkeys((lista)))



#-----------

#----------- LIMPIEZA DE LAS ACEPCIONES
print("\n\tLimpiando de acepciones...")
for i in range(1,101):

    VECS.at[i,'VOC'] = re.sub(LIMP, '', ASE.at[i,'VOC']).lower() #limpieza de vocablos
    lemas.append(VECS.at[i,'VOC'])
    tokdat = re.sub(PARENTESIS, '', ASE.at[i,'ASVO']).lower()
    tokdat = re.sub(LIMP, '', tokdat)
    y = []

    for x in data_complejas:
        sb = x[0].replace(" ","_")
        tokdat = re.sub(x[0],sb,tokdat.rstrip())
    tokdat = tokdat.split(" ")

    for palabra in tokdat:#lista de paro
        if palabra not in lista:
            y.append(palabra)
        else:
            pass
    y = " ".join(y)
    y = limpieza(y).lstrip()
    VECS.at[i,'AVOCL'] = y
    ace.append(y)


#--sinonimo
    VECS.at[i,'SIN'] = re.sub(LIMP, '', ASE.at[i,'SIN']).lower() #limpieza de sinonimos
    lemas.append(VECS.at[i,'SIN'])
    tokdat = re.sub(PARENTESIS, '', ASE.at[i,'ASSI']).lower()
    tokdat = re.sub(LIMP, '', tokdat)
    y = []

    for x in data_complejas:
        sb = x[0].replace(" ","_")
        tokdat = re.sub(x[0],sb,tokdat.rstrip())
    tokdat = tokdat.split(" ")

    for palabra in tokdat:#lista de paro
        if palabra not in lista:
            y.append(palabra)
        else:
            pass
    y = " ".join(y)
    y = limpieza(y).lstrip()
    VECS.at[i,'ASINL'] = y
    ace.append(y)


#--antonimo
    VECS.at[i,'ANT'] = re.sub(LIMP, '', ASE.at[i,'ANT']).lower() #limpieza de antonimos
    lemas.append(VECS.at[i,'ANT'])
    tokdat = re.sub(PARENTESIS, '', ASE.at[i,'ASAN']).lower()
    tokdat = re.sub(LIMP, '', tokdat)
    y = []

    for x in data_complejas:
        sb = x[0].replace(" ","_")
        tokdat = re.sub(x[0],sb,tokdat.rstrip())
    tokdat = tokdat.split(" ")

    for palabra in tokdat: #lista de paro
        if palabra not in lista:
            y.append(palabra)
        else:
            pass
    y = " ".join(y)
    y = limpieza(y).lstrip()
    VECS.at[i,'AANTL'] = y
    ace.append(y)

#-----------

#---------- crear tuplas para meter a la funcion de similitud
print("\n\tCreando lista de tuplas (acepciones y lemas) que alimentaran el calculador de coseno...")
lista_tuplas = []
lista_tU = []
base = 0
contador = 0

while contador < len(ASE):
    lista_tuplas.append((ace[base],ace[base+1])) #usando las definiciones como KEYS para funcion de coseno
    lista_tuplas.append((ace[base],ace[base+2]))
    lista_tuplas.append((ace[base+1],ace[base+2]))

    lista_tU.append((lemas[base],lemas[base+1])) #usando las entradas del diccionario ordenadas para comparacion, otra tupla para coseno
    lista_tU.append((lemas[base],lemas[base+2]))
    lista_tU.append((lemas[base+1],lemas[base+2]))

    base = base + 3
    contador = contador + 1
#-----------


matriz = matriz_de_conteo(ace)

cos = coseno(lista_tuplas, matriz) #toma las columnas como vectores (que es lo normal)

matriz = matriz.transpose() #la cambia, porque el SVD coma la fila como el vector (que sería lo raro, yo que se), además el otro archivo MatPlot toma conjuntos de coordenadas como si
#el vector fuera fila, así que esto está bien dejarlo así para que matplot.py lea bien las coordenadas 3D de cada vector.

U, s, Vh = np.linalg.svd(matriz, full_matrices=False) #rows


#---------- calculo de su coseno desde SVD

manipulacion_de_U = pd.DataFrame(U).transpose() #columnas

manipulacion_de_U.columns = lemas

print ("\tPreparando para cálculo de coseno usando SVD...")
cosSVD = coseno(lista_tU, manipulacion_de_U)

#-------------

#----------- acomodar datos de coseno en tabla nueva
base = 1
contador = 0

while base < len(ASE)+1:
    VECS.at[base,'V0C+SIN'] = cos.at[contador,'COS']
    VECS.at[base,'VOC+ANT'] = cos.at[contador+1,'COS']
    VECS.at[base,'SIN+ANT'] = cos.at[contador+2,'COS']

    VECS.at[base,'V+S_SVD'] = cosSVD.at[contador,'COS']
    VECS.at[base,'S+A_SVD'] = cosSVD.at[contador+1,'COS']
    VECS.at[base,'V+A_SVD'] = cosSVD.at[contador+2,'COS']

    base = base + 1
    contador = contador + 3

#-----------

RECON = np.dot(np.dot(U, np.diag(s)), Vh) #reconstruccion de la matriz, con esto verificamos que realmente se trate de cada definición del diccionario

a = []
for elem in s:
    a.append(elem/np.sum(s)*100)
s = pd.DataFrame({'S':s, 'CUMSUM':np.cumsum(a)})

print ("\tGuardando resultados en Excel...")
with pd.ExcelWriter('MC_transpuesta.xlsx') as writer:
    matriz.to_excel(writer, sheet_name='MatrizdeConteo')
    pd.DataFrame(U).to_excel(writer, sheet_name='U') #recuerda pasar la matriz (array) a dataframe
    s.to_excel(writer, sheet_name='s')
    pd.DataFrame(Vh).to_excel(writer, sheet_name='Vh')
    pd.DataFrame(RECON).to_excel(writer, sheet_name='RECON')
    cos.to_excel(writer, sheet_name='COS_simple')
    cosSVD.to_excel(writer, sheet_name='COS_SVD')
    VECS.to_excel(writer, sheet_name='ANALISIS')
print ("LSA_DEM: Proceso completado")

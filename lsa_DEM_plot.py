
import pandas as pd
import numpy as np
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt

ACE = pd.read_excel('MC_transpuesta.xlsx', 'U', index_col=0)
NOMBRES = pd.read_excel('sinant.xlsx', index_col=0)
n = 0
num = 0

while n < 300:
    print ("\tTrazando la acepción",n,"de un total de 300...", end='\r')
    X = ACE.iloc[n:n+3,0]
    Y = ACE.iloc[n:n+3,1]
    Z = ACE.iloc[n:n+3,2]
    label = [NOMBRES.iloc[num,0], NOMBRES.iloc[num,2], NOMBRES.iloc[num,4]]

    fig = plt.figure("3d")
    ax = plt.axes(projection="3d")
    ax.scatter(X , Y , Z,  color='green')
    for i,type in enumerate(label):
        x = X[i+n]
        y = Y[i+n]
        z = Z[i+n]
        ax.text(x, y, z, type, fontsize=12)
    ax.view_init(elev=13, azim=-150)
    etiqueta = "".join(label)
    num = num + 1
    plt.savefig('plots3d/'+str(num)+etiqueta+'.png')
    fig.clear()

    fig = plt.figure("2d")
    plt.scatter(X, Y, s=100, color="green")
    for i,type in enumerate(label):
        x = X[i+n]
        y = Y[i+n]
        plt.text(x, y, type, fontsize=12)
    plt.savefig('plots2d/'+str(num)+etiqueta+'.png')
    fig.clear()
    n = n+3

# LSA_DEM

Experimentos relacionados con el Diccionario del Español de México de El Colegio de México y la representación vectorial de sus acepciones.

Si usas alguno de los recursos favor de citar de la siguiente manera:

Sánchez Fernández, M. A. y Medina, U. A. (2019). Recursos del Experimento de comparación de definiciones del Diccionario del Español de México mediante la aplicación de LSA. En [URL] Consultado el [].

Diccionario del Español de México (DEM) http://dem.colmex.mx, El Colegio de México, A.C., [fecha de consulta].

El archivo final se encuentra descargable aquí: https://bit.ly/2kfmpxG

Trabajo presentado en el IX Coloquio de Lingüística Computacional, México, CDMX. Universidad Nacional Autónoma de México, Facultad de Filosofía y Letras.
http://www.colico.unam.mx/index.php

